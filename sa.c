#include <stdlib.h>
#include <stdio.h>

#include "readpng.c"
#include "armath.c"

void die() {
    printf("\
usage: structuralascii <image>\n\
  - supported images are: png\n");
    exit(1);
}

int main(int argc, char **argv) {
    if (argc < 2)
        die();

    printf("structural ascii 0.1\n");
    printf("  file %s\n", argv[1]);

    FILE* img = fopen(argv[1], "r");
    if (img == NULL) {
        fprintf(stderr, "  error while opening file for reading\n");
        return 1;
    }

    /* print stats about loaded image */
    readpng_version_info();

    long width;
    long height;

    int ret = readpng_init(img, &width, &height);
    if (ret) {
        fprintf(stderr, "  error while initializing readpng\n");
        return 2;
    }

    printf("file width/height: %ldx%ld\n", width, height);

    long arw = width;
    long arh = height;
    simplifyfractionl(&arw, &arh);
    printf("file aspect ratio: %ld:%ld\n", arw, arh);

    unsigned char rbg;
    unsigned char gbg;
    unsigned char bbg;
    readpng_get_bgcolor(&rbg, &gbg, &bbg);

    printf("file background: %i, %i, %i\n", rbg, gbg, bbg);

    return 0;
}
