OPTIONS:=
OPTIONS+= -l m
OPTIONS+= -l png

sa: sa.c
	gcc -Wall -std=c99 -pedantic -g sa.c -o sa $(OPTIONS)

clean:
	rm -rf sa
