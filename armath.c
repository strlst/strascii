#include <stdio.h>

int gcd(int, int);
long gcdl(long, long);
void simplifyfraction(int*, int*);
void simplifyfractionl(long*, long*);

/* god bless euclid */
int gcd(int a, int b) {
    int t;
    while (b) {
        t = a;
        a = b;
        b = t % b;
        //printf("t %i a %i b %i\n", t, a, b);
    }
    return a;
}

/* long variant */
long gcdl(long a, long b) {
    long t;
    while (b) {
        t = a;
        a = b;
        b = t % b;
        //printf("t %i a %i b %i\n", t, a, b);
    }
    return a;
}

/* set the pointed address of an existing fraction to its simplified version */
void simplifyfraction(int *num, int *denom) {
    int n = *num;
    int d = *denom;

    /* check for div by 0 */
    if (d == 0) {
        fprintf(stderr, "division by 0, result undefined\n");
        exit(1);
    }

    /* get gcd */
    int comdiv = gcd(n, d);

    /* reduce */
    n = n / comdiv;
    d = d / comdiv;

    *num = n;
    *denom = d;
}

/* long variant */
void simplifyfractionl(long *num, long *denom) {
    long n = *num;
    long d = *denom;

    /* check for div by 0 */
    if (d == 0) {
        fprintf(stderr, "division by 0, result undefined\n");
        exit(1);
    }

    /* get gcd */
    long comdiv = gcd(n, d);

    /* reduce */
    n = n / comdiv;
    d = d / comdiv;

    *num = n;
    *denom = d;
}
